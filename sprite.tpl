// Generated at {{date}} by svg-sprite with custom sass template

.icon
  fill: currentColor
  height: 1em

{{#shapes}}
.{{name}}
  width: ({{width.outer}} / {{height.outer}}) * 1em

{{/shapes}}
